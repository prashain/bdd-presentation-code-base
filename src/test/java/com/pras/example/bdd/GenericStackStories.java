package com.pras.example.bdd;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.steps.CandidateSteps;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class GenericStackStories extends AbstractJUnitStories {

	public GenericStackStories() {
		{
			configuredEmbedder().embedderControls()
					.doGenerateViewAfterStories(true)
					.doIgnoreFailureInStories(true)
					.doIgnoreFailureInView(false).doVerboseFailures(true)
					.useThreads(2).useStoryTimeoutInSecs(60).verboseFailures();
			// configuredEmbedder().useEmbedderControls(new
			// PropertyBasedEmbedderControls());
		}
	}

	@Override
	protected List<String> storyPaths() {
		return Arrays.asList("com/pras/example/bdd/GenericStack.story");
	}

    @Override public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new GenericStackSteps());
    }

	public List<CandidateSteps> candidateSteps(){
		return new InstanceStepsFactory(configuration(), new GenericStackSteps()).createCandidateSteps();

	}



}
