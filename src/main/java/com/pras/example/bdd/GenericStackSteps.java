package com.pras.example.bdd;

import static org.fest.assertions.Assertions.assertThat;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class GenericStackSteps {


	private GenericStack<String> myStack;
	private String _lastPushedElement;
	private int sizeOfStack;
	private boolean isStackEmpty;

	@Given("I have a stack")
	public void given_a_new_stack() {
		myStack = new GenericStack<String>();
	}

	@When("I push an element")
	public void iPushAnElement() {
		myStack.push("tmp");
	}

	@Then("Stack should have size $size")
	public void theSizeOfStackShouldBe(@Named("value") int size) {
		assertThat(myStack.getSize()).isEqualTo(size);
	}

	@When("I pop an element")
	public void iPopTheStack() {
		myStack.push("tmp");
		_lastPushedElement = myStack.pop();
	}

	@Then("I should get last pushed element")
	public void shouldGetLastPushedElement() {
		assertThat(_lastPushedElement).isNotNull();
	}

	@Then("stack size should be decremented")
	public void andStackSizeShouldBeDecremented() {
		assertThat(myStack.getSize()).isEqualTo(0);
	}

	@Given("I have a stack with no element")
	public void givenAnEmptyStack(){
		myStack = new GenericStack<String>();
	}

	@When("I pop an element off empty stack")
	public void iPopOffemptyStack() {
		_lastPushedElement = myStack.pop();
	}

	@Then("I should get null")
	public void shouldGetNullAsReturn() {
		assertThat(_lastPushedElement).isNull();
	}

	@When("I query for an empty stack")
	public void whenQueryForEmptyStackWthFreshStack(){
		isStackEmpty= myStack.isEmpty();
	}


	@Then("I should get returnValue true")
	public void shouldVerifyEmptyStackWthFreshStack(){
		assertThat(isStackEmpty).isTrue();
	}


	@Given("I have a stack with some elements")
	public void stackWithFewElements(){
		myStack = new GenericStack<String>().push("foo").push("bar").push("baz");
	}

	@When("I peek stack")
	public void peekStack(){
		_lastPushedElement = myStack.peek();
		sizeOfStack = myStack.getSize();
	}
	@Then("I should get last pushed element in stack")
	public void shouldGetLastPushedElementInStack(){
		assertThat(_lastPushedElement).isEqualTo("baz");
	}
	@Then("stack size should not be modified")
	public void shouldNotAlterSizeOfStack(){
		assertThat(sizeOfStack).isEqualTo(3);
	}

	@Given("I have a stack with full capacity")
	public void givenAStackWithFullCapacity(){
		myStack = new GenericStack<String>().push("foo").push("bar").push("baz").push("raz");
	}

	@Then("stack size should increase")
	public void shouldIncreaseStackSize(){
		assertThat(myStack.getSize()).isGreaterThan(4);
	}
	@Then("peek should give me element last pushed")
	public void shouldVerifyThatLastElmentPushedExistsInStack(){
		assertThat(myStack.peek()).isEqualTo("tmp");
	}




}
