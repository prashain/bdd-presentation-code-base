package com.pras.example.bdd;

public class GenericStack<T> {

	private T[] _storage;
	private int size ;

	@SuppressWarnings("unchecked")
	// Default storage is 4 units.
	public GenericStack(){
		_storage = (T[]) new Object[4];
		size=0;
	}

	/**
	 *
	 * @param Element to add into stack
	 * @return altered stack.
	 */
	public GenericStack<T> push(T element) {
		ensureCapacity();
		_storage[size] = element;
		size++;
		return this;
	}

	/**
	 *  Removes and returns the last entered value in stack.
	 * @return last pushed value.
	 */
	public T pop(){
		T poppedElement = null;
		if(size > 0){
			poppedElement = _storage[(size -1)];
			size--;
		}
		return poppedElement;
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		return size==0;

	}

	public T peek() {
		T poppedElement = null;
		if(size >= 0)
			poppedElement = _storage[(size -1)];
		return poppedElement;
	}

	@SuppressWarnings("unchecked")
	private void ensureCapacity(){
		int newCapacity;
		int oldCapacity = _storage.length;
		if(oldCapacity == size){
			newCapacity = (oldCapacity * 3)/2 + 1;
			T[] _newStorage = (T[]) new Object[newCapacity];
			System.arraycopy(_storage, 0, _newStorage, 0, _storage.length);
			_storage = _newStorage;
		}
		else
			newCapacity = size;
	}

}
