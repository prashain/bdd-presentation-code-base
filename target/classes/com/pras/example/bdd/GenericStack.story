Feature: GenericStack
        In order to have LIFO behavior
        As a developer
        I want to have a datastructure to emulate Stack


Scenario: 	Push element into stack
Given I have a stack
When I push an element
Then Stack should have size 1


Scenario: Pop an element off stack
Given I have a stack
When I pop an element
Then I should get last pushed element
And stack size should be decremented

Scenario: Pop an element off an empty stack
Given I have a stack with no element
When I pop an element off empty stack
Then I should get null


Scenario: Check whether stack is empty
Given I have a stack with no element
When I query for an empty stack
Then I should get returnValue true

Scenario: Get top element in  stack
Given I have a stack with some elements
When I peek stack
Then I should get last pushed element in stack
And stack size should not be modified

Scenario: Push an element into full stack
Given I have a stack with full capacity
When I push an element
Then stack size should increase
And peek should give me element last pushed


